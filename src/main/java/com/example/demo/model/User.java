package com.example.demo.model;

public class User {
 private int id;
 private String name;
 private String passward;


public User(int id, String name, String passward) {
	super();
	this.id = id;
	this.name = name;
	this.passward = passward;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPassward() {
	return passward;
}

public void setPassward(String passward) {
	this.passward = passward;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}
}
